import React, { Component } from 'react';
import LoginForm from '../component/Login';
import RegisterForm from '../component/Register';
import ForgotPasswordForm from '../component/ForgotPassword';
import NewPasswordForm from '../component/NewPassword';

class Home extends Component {
  constructor() {
    super();
    this.state = {
      currentPage: 'LoginForm',
    };
  }

  handleForgotPasswordClick = () => {
    this.setState({ currentPage: 'ForgotPasswordForm' });
  };

  handleRegisterClick = () => {
    this.setState({ currentPage: 'RegisterForm' });
  };

  handleLoginClick = () => {
    this.setState({ currentPage: 'LoginForm' });
  };

  handleNewPasswordClick = () => {
    this.setState({ currentPage: 'NewPasswordForm' });
  };

  renderCurrentPage() {
    if (this.state.currentPage === 'LoginForm') {
      return <LoginForm onForgotPasswordClick={this.handleForgotPasswordClick} onRegisterClick={this.handleRegisterClick} />;
    } else if (this.state.currentPage === 'RegisterForm') {
      return <RegisterForm onLoginClick={this.handleLoginClick} />;
    } else if (this.state.currentPage === 'ForgotPasswordForm') {
      return <ForgotPasswordForm onLoginClick={this.handleLoginClick} onNewPasswordClick={this.handleNewPasswordClick} />;
    } else if (this.state.currentPage === 'NewPasswordForm') {
      return <NewPasswordForm onLoginClick={this.handleLoginClick} />;
    }
  }

  render() {
    return (
      <div id="Section">
        <main id="MainSection">
          <img src="/MainSection.png" alt="" />
        </main>
        {this.renderCurrentPage()}
      </div>
    );
  }
}

export default Home;
