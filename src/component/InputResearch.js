import { useState } from 'react';
import InputResearchForm from './InputResearchForm';
import Manager from './Manager';
import api from '../api/api';
import { ToastContainer, toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

const intialFields = {
  title_research: '',
  background_research: '',
  category_research: '',
  goal_research: '',
  assign_research: '',
};

export default function InputResearch({ onAsignManagerClick }) {
  const [isEdit, setIsEdit] = useState(false);
  const [fields, setFields] = useState(intialFields);
  const [isAssignManager, setIsAssignManager] = useState(false);

  const navigate = useNavigate();

  const handleEdit = () => {
    setIsEdit(!isEdit);
  };

  const handleClickAssignManager = () => {
    setIsAssignManager(true);
  };

  const handleChange = (e) => {
    setFields((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const resetForm = () => {
    setFields(intialFields);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await api.post(`/add-research`, fields);
      if (response.status === 200) {
        resetForm();
        toast.success(response.data.message);
        navigate('/Dashboard');
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        {isAssignManager ? (
          <Manager fields={fields} setFields={setFields} />
        ) : (
          <main className="flex flex-col justify-center items-center">
            <div id="mHzceCSec5">
              <div className="flex" id="rXTIriwHgz">
                {isEdit ? <input type="text" name="title_research" onChange={handleChange} /> : <p>Inisisasi Judul Research</p>}
                <img src="/assets/images/Edit.png" width={24} height={24} onClick={handleEdit} />
              </div>
              <img src="/assets/images/Add User.png" width={37} height={37} />
            </div>
            <div id="ZdU8QiECzO" style={{ backgroundImage: 'url(/assets/images/Image.png)' }}></div>
            {/* <InputResearchForm onAsignManagerClick={onAsignManagerClick} onChange={handleChange} onSubmit={handleSubmit} />
             */}
            <div id="gHd1cwFNxB">
              <label for="LatarBelakang" className="font-Roboto text-24px">
                Latar Belakang Research*
              </label>
              <textarea
                name="background_research"
                onChange={handleChange}
                placeholder="Jelaskan latar belakang/permasalahan yang dihadapi"
              ></textarea>
              <label for="LatarBelakang" className="font-Roboto text-24px">
                Kategori Research
              </label>
              <textarea
                name="category_research"
                onChange={handleChange}
                type="text"
                placeholder="Ilmiah / Sejarah / Hukum / Sosial / Budaya"
              ></textarea>
              <label for="LatarBelakang" className="font-Roboto text-24px">
                Goals Research
              </label>
              <textarea name="goal_research" onChange={handleChange} type="text" placeholder="Tentukan tujuan/hasil dari research"></textarea>
              <p className="font-Roboto text-20px">*Wajib diisi</p>
              <div>
                <button type="button">Cancel</button>
                <button type="submit">Save</button>
                <button type="button" onClick={handleClickAssignManager}>
                  Asign a Manager
                </button>
              </div>
            </div>
          </main>
        )}
      </form>

      <ToastContainer />
    </>
  );
}
