import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { ToastContainer } from 'react-toastify';

const RegisterForm = ({ onLoginClick }) => {
  const initalField = {
    name: '',
    email: '',
    phone: '',
    password: '',
    institution_id: '',
    type_user_id: '',
  };

  // state untuk fields inputan
  const [field, setField] = useState(initalField);
  const [loadingSubmitted, setLoadingSubmitted] = useState(false);

  // state list master
  const [listIntitusi, setListInstitusi] = useState([]);
  const [listRole, setListRole] = useState([]);

  function HandleEyeIcon() {
    let password = document.querySelector('.Password');
    let eyeicon = document.getElementById('EyeIcon');

    if (password.type === 'password') {
      password.type = 'text';
      eyeicon.src = '/assets/images/eye-open.png';
    } else {
      password.type = 'password';
      eyeicon.src = '/assets/images/eye-close.png';
    }
  }

  function HandleEyeIconRe() {
    let password = document.querySelector('.PasswordRe');
    let eyeicon = document.getElementById('EyeIconRe');

    if (password.type === 'password') {
      password.type = 'text';
      eyeicon.src = '/assets/images/eye-open.png';
    } else {
      password.type = 'password';
      eyeicon.src = '/assets/images/eye-close.png';
    }
  }

  async function getInstitusi() {
    try {
      const response = await axios.get(process.env.REACT_APP_BE_URL + '/institution');
      const result = response.data;
      setListInstitusi(result);
    } catch (error) {
      console.error(error);
    }
  }

  async function getRole() {
    try {
      const response = await axios.get(process.env.REACT_APP_BE_URL + '/role');
      const result = response.data;
      setListRole(result);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    getInstitusi();
    getRole();
  }, []); // manggil data role ketika pertamakali di load tampilan register

  const handleChange = (e) => {
    setField({
      ...field,
      [e.target.name]: e.target.value,
    });
  };

  const resetForm = () => {
    setField(initalField);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    setLoadingSubmitted(true);

    try {
      const response = await axios.post(process.env.REACT_APP_BE_URL + '/auth/register', field);
      const result = response.data;
      if (response.status === 200) {
        resetForm();
        toast.success(result.message);
      }
    } catch (error) {
      toast.error(error.response.data.message);
    } finally {
      setLoadingSubmitted(false);
    }
  };

  return (
    <>
      <aside id="RegisterForm">
        <p>AMD for Research</p>
        <p id="DetailRegister">Silakan masukkan data Anda untuk membuat Akun</p>
        <form onSubmit={handleSubmit}>
          <div id="InputGroup">
            <label for="name" id="InputLabel">
              Nama Lengkap
            </label>
            <input type="text" id="InputForm" name="name" placeholder="First Last" onChange={handleChange}></input>
          </div>
          <div id="InputGroup">
            <label for="email" id="InputLabel">
              Email Corporate
            </label>
            <input type="email" id="InputForm" name="email" placeholder="you@email.com" onChange={handleChange}></input>
          </div>
          <div id="InputGroup">
            <label for="phone" id="InputLabel">
              Nomor Handphone
            </label>
            <input type="text" id="InputForm" name="phone" placeholder="08xxxxxx" onChange={handleChange}></input>
          </div>
          <div id="InputGroup">
            <label for="Intuisi" id="InputLabel">
              Intuisi
            </label>
            <select id="InputSelect" name="institution_id" onChange={handleChange}>
              <option value="">Pilihan</option>
              {listIntitusi.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.label}
                </option>
              ))}
            </select>
          </div>
          <div id="InputGroup">
            <label for="type_user_id" id="InputLabel">
              Tipe User
            </label>
            <select id="InputSelect" name="type_user_id" onChange={handleChange}>
              <option value="">Pilihan</option>
              {listRole.map((item) => (
                <option key={item.id} value={item.id}>
                  {item.label}
                </option>
              ))}
            </select>
          </div>
          <div id="InputGroup">
            <label for="password" id="InputLabel">
              Password
            </label>
            <div className="flex" id="InputPassword">
              <input type="password" id="InputForm" className="Password" name="password" placeholder="•••••••••" onChange={handleChange}></input>
              <img src="/assets/images/eye-close.png" alt="" width="26px" id="EyeIcon" onClick={HandleEyeIcon}></img>
            </div>
          </div>
          <div id="InputGroup">
            <label for="Re-Password" id="InputLabel">
              Re-Type Password
            </label>
            <div className="flex" id="InputPassword">
              <input type="password" id="InputForm" className="PasswordRe" name="Re-Password" placeholder="•••••••••"></input>
              <img src="/assets/images/eye-close.png" alt="" width="26px" id="EyeIconRe" onClick={HandleEyeIconRe}></img>
            </div>
          </div>
          <button disabled={loadingSubmitted} type="submit" id="SignButton">
            {loadingSubmitted ? 'loading ...' : 'Create an Account'}
          </button>
          <div className="flex" id="qDSFE733Q">
            <p>Sudah Punya Akun?</p>
            <p onClick={onLoginClick}>Sign In</p>
          </div>
        </form>
      </aside>

      <ToastContainer />
    </>
  );
};

export default RegisterForm;
