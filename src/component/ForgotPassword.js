import React from 'react'

const ForgotPasswordForm = ({ onLoginClick, onNewPasswordClick }) => {
  return (
        <aside id='LoginForm'>
            <p>Forgot Password?</p>
            <p>Silahkan masukkan Email Anda</p>
            <div id='InputGroupForgot'>
                <label for="Email" id='InputLabel'>Email Corporate</label>
                <input type='email' id='InputForm' name='Email' placeholder='you@email.com'></input>
            </div>
            <button id='SignButton' onClick={onNewPasswordClick}>Reset Password</button>
            <div className='flex' id='qDSFE733Q'>
                <p>Back to login page?</p>
                <p onClick={onLoginClick}>Sign In.</p>
            </div>
        </aside>
  )
}

export default ForgotPasswordForm