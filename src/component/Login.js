import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';
import api from '../api/api';

const LoginForm = ({ onForgotPasswordClick, onRegisterClick }) => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [field, setField] = useState({
    email: '',
    password: '',
  });

  function HandleEyeIcon() {
    let password = document.querySelector('.Password');
    let eyeicon = document.getElementById('EyeIcon');

    if (password.type === 'password') {
      password.type = 'text';
      eyeicon.src = '/assets/images/eye-open.png';
    } else {
      password.type = 'password';
      eyeicon.src = '/assets/images/eye-close.png';
    }
  }

  const handleChange = (e) => {
    setField({
      ...field,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    const token = Cookies.get('token');

    if (token) {
      navigate('/Dashboard');
    }
  }, [navigate]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    setLoading(true);

    try {
      const response = await api.post('/auth/login', field);
      const result = response.data;
      const token = result.access_token;

      if (response.status === 200) {
        Cookies.set('token', token);

        // arahkan  ke halaman after login
        navigate('/Dashboard'); // tambahkan disini
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <aside id="LoginForm">
      <p>AMD for Research</p>
      <p>Silahkan masuk ke Akun Anda</p>
      <form onSubmit={handleSubmit}>
        <div id="InputGroup">
          <label htmlFor="email" id="InputLabel">
            Email Corporate
          </label>
          <input type="email" id="InputForm" name="email" placeholder="you@email.com" value={field.email} onChange={handleChange} />
        </div>
        <div id="InputGroup">
          <label htmlFor="password" id="InputLabel">
            Password
          </label>
          <div className="flex" id="InputPassword">
            <input
              type="password"
              id="InputForm"
              className="Password"
              name="password"
              placeholder="•••••••••"
              value={field.password}
              onChange={handleChange}
            />
            <img src="/assets/images/eye-close.png" alt="" width="26px" id="EyeIcon" onClick={HandleEyeIcon}></img>
          </div>
        </div>
        <div className="flex" id="ZqPnjN2v1S">
          <div className="flex">
            <input type="checkbox" id="InputCheckbox" name="RememberMe"></input>
            <label for="RememberMe" id="CheckboxLabel">
              Remember Me
            </label>
          </div>
          <p onClick={onForgotPasswordClick}>Forgot Password?</p>
        </div>
        <button disabled={loading} type="submit" id="SignButton">
          {loading ? 'loading ...' : 'Sign In'}
        </button>
        <div className="flex" id="qDSFE7331Q">
          <p>Belum Punya Akun?</p>
          <p onClick={onRegisterClick}>Create an account.</p>
        </div>
      </form>
    </aside>
  );
};

export default LoginForm;
